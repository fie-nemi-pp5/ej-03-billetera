class Personalidad:
    def __init__(self):
        self.persona = None

    def set_persona(self, persona):
        if self.persona is None:
            self.persona = persona

    def puede_prestar(self, per):
        pass


class DenominacionIncorrecta(BaseException):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "No existe la denominacion ${}".format(self.value)


class Billetera:
    def __init__(self, billetes):
        self.billetes = billetes

    def validar(self, billete):
        if billete not in [10, 20, 50, 100, 200, 500, 1000, 2000]:
            raise (DenominacionIncorrecta(billete))

    def hay_de(self, denominacion):
        self.validar(denominacion)
        return denominacion in self.billetes

    def poner_billete(self, denominacion):
        self.validar(denominacion)
        self.billetes.append(denominacion)

    def sacar_billete(self, denominacion):
        self.billetes.remove(denominacion)


class Persona:
    def __init__(self, nombre, personalidad: Personalidad, billetera=Billetera([])):
        self.nombre = nombre
        self.personalidad = personalidad
        self.personalidad.set_persona(self)
        self.amigos = []
        self.billetera = billetera

    def agregar_amigo(self, persona):
        if persona not in self.amigos:
            self.amigos.append(persona)

    def es_amigo(self, persona):
        return True if persona in self.amigos else False

    def prestar(self, monto, persona):
        if self.personalidad.puede_prestar(persona) and self.billetera.hay_de(monto):
            self.billetera.sacar_billete(monto)
            return True

    def pedir_plata(self, monto, persona):
        if persona.prestar(monto, self):
            self.billetera.poner_billete(monto)
            print("Me prestaron ${}".format(monto))
        else:
            print("No me prestaron ${}".format(monto))


class Macanudo(Personalidad):
    def puede_prestar(self, persona):
        return True


class Amigazo(Personalidad):
    def puede_prestar(self, persona):
        return True if self.persona.es_amigo(persona) else False


class Codito(Personalidad):
    def puede_prestar(self, persona):
        return False


juan = Persona("Juan", Amigazo())
pedro = Persona("Pedro", Amigazo(), billetera=Billetera([100, 100]))
pedro.agregar_amigo(juan)
juan.pedir_plata(20, pedro)
juan.pedir_plata(100, pedro)
juan.pedir_plata(100, pedro)
juan.pedir_plata(100, pedro)
# juan.pedir_plata(2, pedro)
